import 'package:flutter/material.dart';
import 'package:blood_finder/core/presentation/widgets/Button.dart';
import 'package:blood_finder/core/presentation/widgets/donate_image.dart';
import 'package:blood_finder/core/presentation/widgets/painter.dart';

class StartScreen extends StatelessWidget {
  const StartScreen();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LayoutBuilder(builder: (context, constraints) {
        final height = constraints.maxHeight;
        final width = constraints.maxWidth;
        return Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                CustomPaint(
                  size: Size(double.infinity, constraints.maxHeight * .5),
                  painter: Painter(),
                ),
                SizedBox(
                  width: width * .99,
                  height: height * .52,
                  child: DonateImage(),
                ),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Text(
                  '"Every blood donor is a life saver"',
                  style: TextStyle(fontSize: 20),
                ),
                SizedBox(height: height * .085),
                Button(
                  text: "Become Donor",
                  width: 300,
                  onPressed: () {},
                  color: Theme.of(context).primaryColor,
                ),
                SizedBox(height: height * .03),
                Button(
                  text: "Request for Blood",
                  width: 300,
                  onPressed: () {},
                  color: Theme.of(context).primaryColor,
                ),
              ],
            ),
          ],
        );
      }),
    );
  }
}
