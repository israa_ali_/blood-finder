import 'package:flutter/material.dart';
import 'package:blood_finder/core/presentation/widgets/logo.dart';
import 'package:blood_finder/core/presentation/widgets/waves.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          backgroundColor: Theme.of(context).primaryColor,
          body: LayoutBuilder(builder: (context, Constraints) {
            final height = Constraints.maxHeight;
            final width = Constraints.maxWidth;

            return Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Spacer(),
                Logo(),
                Spacer(),
                Waves(
                  height: height,
                  width: width,
                ),
              ],
            );
          })),
    );
  }
}
