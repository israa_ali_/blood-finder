import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';

class Waves extends StatelessWidget {
  const Waves({this.height, this.width});
  final height, width;
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      alignment: Alignment.bottomCenter,
      height: height * .3,
      width: width,
      child: FlareActor(
        'assets/waves.flr',
        fit: BoxFit.fill,
        animation: "NewBackgroundLoop",
      ),
    );
  }
}
