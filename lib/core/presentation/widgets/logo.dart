import 'package:flutter/material.dart';

class Logo extends StatelessWidget {
  const Logo();

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Text("B",
          style: TextStyle(
            fontSize: 90,
            color: Colors.white,
            fontWeight: FontWeight.w800,
          )),
    );
  }
}
