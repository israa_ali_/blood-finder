import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  @override
  final double width;
  final VoidCallback onPressed;
  final String text;
  final Color color;
  const Button({this.onPressed, this.text, this.width, this.color});

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
      minWidth: width,
      height: 60.0,
      child: RaisedButton(
        child: Text(
          text,
          style: TextStyle(fontSize: 24, fontFamily: 'Roboto'),
        ),
        textColor: Colors.white,
        color: color,
        onPressed: onPressed,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25.0),
        ),
      ),
    );
  }
}
