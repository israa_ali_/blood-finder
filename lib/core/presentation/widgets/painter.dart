import 'package:flutter/material.dart';

class Painter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final width = size.width, height = size.height;
    final paint = Paint()
      ..color = const Color.fromARGB(255, 176, 39, 33)
      ..strokeWidth = 4
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.fill;

    canvas.drawCircle(Offset(width / 2, height / 2), 5, paint);
    final path = Path()
      ..lineTo(0, height)
      ..quadraticBezierTo(
        width / 2,
        height * .6,
        width,
        height,
      )
      ..lineTo(width, 0)
      ..close();

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
