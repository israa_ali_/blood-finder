import 'package:blood_finder/core/presentation/screens/splash_screen.dart';
import 'package:flutter/material.dart';

class AppBase extends StatelessWidget {
  const AppBase();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: const Color.fromARGB(255, 176, 39, 33),
      ),
      home: SplashScreen(),
    );
  }
}
